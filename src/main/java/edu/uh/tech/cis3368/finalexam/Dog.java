package edu.uh.tech.cis3368.finalexam;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

@Entity
public class Dog {

    private int id;
    private String name;
    private String breed;
    private double weight;

    private Set<Flea> fleas = new HashSet<>();

    @OneToMany(mappedBy = "dog", cascade = CascadeType.ALL, orphanRemoval = true,fetch = FetchType.EAGER)
    public Set<Flea> getFleas() {
        return fleas;
    }

    public void setFleas(Set<Flea> fleas) {
        this.fleas = fleas;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "BREED")
    public String getBreed() {
        return breed;
    }

    public void setBreed(String breed) {
        this.breed = breed;
    }

    @Column(name = "WEIGHT")
    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    @Override
    public String toString() {
        return "Dog{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", breed='" + breed + '\'' +
                ", weight=" + weight +
                '}';
    }

    public void addFlea(Flea flea) {
        fleas.add(flea);
        flea.setDog(this);
    }

    public void removeFlea(Flea flea) {
        fleas.remove(flea);
    }

    public void scratch() {
        fleas.forEach(flea -> flea.setDog(null));
        fleas.clear();
    }
}
