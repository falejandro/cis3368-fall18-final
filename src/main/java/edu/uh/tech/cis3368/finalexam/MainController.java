package edu.uh.tech.cis3368.finalexam;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.scene.input.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

@Component
public class MainController implements Initializable {

    @FXML
    private TextField fleaName;

    @FXML
    private ListView fleasOnDog;

    @FXML
    private ListView fleaList;

    @FXML
    private ImageView dogImage;

    @Autowired
    private DogRepository dogRepository;

    @Autowired
    private FleaRepository fleaRepository;

    private static final DataFormat FLEA_LIST = new DataFormat("cis3368/fleaList");
    private Dog dog;

    public void onDragDetected(MouseEvent mouseEvent) {

        int selected = fleaList.getSelectionModel().getSelectedIndices().size();
        System.out.println(String.format("%d selected",selected));
        if(selected > 0){
            Dragboard dragboard = fleaList.startDragAndDrop(TransferMode.MOVE);
            ArrayList<Flea> selectedItems = new ArrayList<>(fleaList.getSelectionModel().getSelectedItems());
            ClipboardContent content = new ClipboardContent();
            content.put(FLEA_LIST,selectedItems);
            dragboard.setContent(content);
            mouseEvent.consume();
        } else {
            System.out.println("nothing selected");
            mouseEvent.consume();
        }


    }

    public void onDragDone(DragEvent dragEvent) {
        System.out.println("Drag done detected");
        TransferMode tm = dragEvent.getAcceptedTransferMode();
        if(tm == TransferMode.MOVE) {
            removeSelectedItems();
        }
        dragEvent.consume();

        fleaRepository.findAll().forEach(System.out::println);
    }

    public void onDragDropped(DragEvent dragEvent) {
        boolean dragCompleted = false;
        Dragboard dragboard = dragEvent.getDragboard();
        if (dragboard.hasContent(FLEA_LIST)) {
            ArrayList<Flea> fleas = (ArrayList<Flea>) dragboard.getContent(FLEA_LIST);
            fleas.forEach(flea -> {
                dog.addFlea(flea);
            });
            dogRepository.save(dog);
            fleasOnDog.getItems().addAll(fleas);
            dragCompleted = true;
        }
        dragEvent.setDropCompleted(dragCompleted);
        dragEvent.consume();
    }

    public void onDragOver(DragEvent dragEvent) {
        Dragboard dragboard = dragEvent.getDragboard();
        if(dragboard.hasContent(FLEA_LIST)){
            dragEvent.acceptTransferModes(TransferMode.MOVE);
        }
        dragEvent.consume();

    }

    /**
     * Called to initialize a controller after its root element has been
     * completely processed.
     *
     * @param location  The location used to resolve relative paths for the root object, or
     *                  {@code null} if the location is not known.
     * @param resources The resources used to localize the root object, or {@code null} if
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        dog = new Dog();
        dog.setName("Henry");
        dog.setBreed("Mutt");
        dog.setWeight(30.24);
        dogRepository.save(dog);

        Flea flea = new Flea();
        flea.setName("Bert");
        fleaRepository.save(flea);
        fleaList.getItems().add(flea);
    }

    private void removeSelectedItems() {
        ObservableList selectedFleas = fleaList.getSelectionModel().getSelectedItems();
        fleaList.getItems().removeAll(selectedFleas);
        fleaList.getSelectionModel().clearSelection();
    }

    public void doScratch(ActionEvent actionEvent) {
        dog.scratch();
        fleasOnDog.getItems().clear();
    }

    public void doAddFlea(ActionEvent actionEvent) {
        Flea flea = new Flea();
        var newFleaName = fleaName.getText();
        if(!newFleaName.isEmpty()) {
            flea.setName(fleaName.getText());
            fleaList.getItems().add(flea);
        }
    }
}
