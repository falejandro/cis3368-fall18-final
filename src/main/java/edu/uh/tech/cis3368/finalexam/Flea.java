package edu.uh.tech.cis3368.finalexam;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Flea implements Serializable {
    private int id;

    private String name;
    private Dog dog;
    @ManyToOne(cascade = CascadeType.PERSIST)
    public Dog getDog() {
        return dog;
    }

    public void setDog(Dog dog) {
        this.dog = dog;
    }


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return "Flea{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", dog=" + dog +
                '}';
    }
}
