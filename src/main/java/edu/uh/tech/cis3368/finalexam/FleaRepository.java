package edu.uh.tech.cis3368.finalexam;

import org.springframework.data.repository.CrudRepository;

import java.util.ArrayList;

public interface FleaRepository extends CrudRepository<Flea,Integer> {
    ArrayList<Flea> findAllByDogId(int id);

}
