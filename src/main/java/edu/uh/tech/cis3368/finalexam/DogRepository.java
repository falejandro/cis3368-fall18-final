package edu.uh.tech.cis3368.finalexam;

import org.springframework.data.repository.CrudRepository;

public interface DogRepository extends CrudRepository<Dog,Integer> {

    Dog findByName(String name);
}
