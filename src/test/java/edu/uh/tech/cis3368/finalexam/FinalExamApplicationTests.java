package edu.uh.tech.cis3368.finalexam;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import javax.transaction.Transactional;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional // causes each test to clean up after itself
public class FinalExamApplicationTests {

    @Autowired
    private DogRepository dogRepository;

    @Autowired
    private FleaRepository fleaRepository;

    @Test
    public void testCreatingADog(){
        Dog dog = new Dog();
        dog.setName("Fido");
        dog.setBreed("Mutt");
        dog.setWeight(4.3);
        dogRepository.save(dog);
        // should be only 1 in the database
        assertEquals(1,dogRepository.count());
    }

    @Test
    public void testADogCanHaveFleas(){
        Dog myDog = new Dog();
        myDog.setName("John");
        myDog.setBreed("Mutt");
        myDog.setWeight(5.2);

        Flea flea = new Flea();
        flea.setName("Tim");
        flea.setDog(myDog);
        myDog.addFlea(flea);
        fleaRepository.save(flea);
        assertEquals(1, myDog.getFleas().size());

        Dog dogReloaded = dogRepository.findByName("John");
        assertNotNull(dogReloaded);
    }

    @Test
    public void testAfleaCanBeRemoved(){
        Dog myDog = new Dog();
        myDog.setName("John");
        myDog.setBreed("Mutt");
        myDog.setWeight(5.2);

        Flea flea = new Flea();
        flea.setName("Tim");
        flea.setDog(myDog);
        myDog.addFlea(flea);
        fleaRepository.save(flea);

        Flea flea2 = new Flea();
        flea.setName("Elbert");
        flea.setDog(myDog);
        myDog.addFlea(flea2);
        fleaRepository.save(flea2);

        Dog dogReloaded = dogRepository.findByName("John");
        dogReloaded.removeFlea(flea2);
        dogRepository.save(dogReloaded);

        // load from db just to make sure
        Dog dogReloadedAgain = dogRepository.findByName("John");
        assertNotNull("dog not found",dogReloadedAgain);
        assertEquals(1, dogReloadedAgain.getFleas().size());
    }

    @Test
    public void testADogWithFleasCanScratchOffAllFleas(){
        Dog myDog = new Dog();
        myDog.setName("John");
        myDog.setBreed("Mutt");
        myDog.setWeight(5.2);

        Flea flea = new Flea();
        flea.setName("Tim");
        flea.setDog(myDog);
        myDog.addFlea(flea);
        fleaRepository.save(flea);


        Flea flea2 = new Flea();
        flea.setName("Harold");
        flea.setDog(myDog);
        myDog.addFlea(flea2);
        fleaRepository.save(flea2);
        assertEquals(2, myDog.getFleas().size());
        myDog.scratch();
        dogRepository.save(myDog);
        assertEquals(0,myDog.getFleas().size());

        // insure that no fleas are assigned to the dog
        assertEquals(0, fleaRepository.findAllByDogId(myDog.getId()).size());
    }

}
